FROM registry.cn-hangzhou.aliyuncs.com/changfeng5285/live-arm-dev
ADD target/live-0.0.1-SNAPSHOT.jar /live.jar
ENTRYPOINT ["java", "-jar", "/live.jar"]