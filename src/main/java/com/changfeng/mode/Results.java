package com.changfeng.mode;

import lombok.Data;

@Data
public class Results<T> {
    private int code;
    private T data;

    public static <T> Results success() {
        Results<T> results = new Results<>();
        results.code = 0;
        return results;
    }

    public static <T> Results success(T data) {
        Results<T> success = success();
        success.data = data;
        return success;
    }

    public static <T> Results error() {
        Results<T> results = new Results<>();
        results.code = 1;
        return results;
    }

    public static <T> Results error(T data) {
        Results<T> err = error();
        err.data = data;
        return err;
    }
}
