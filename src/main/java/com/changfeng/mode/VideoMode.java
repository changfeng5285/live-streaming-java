package com.changfeng.mode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VideoMode {
    private int id;
    private String name;
    private String path;
    boolean state = false;
}
