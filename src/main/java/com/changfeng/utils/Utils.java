package com.changfeng.utils;


import com.changfeng.mode.VideoMode;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class Utils {
    public static List<VideoMode> findVideo(String path) {
        File f = new File(path);
        if (!f.exists()) {
            return null;
        }

        List<VideoMode> result = new ArrayList<>();
        int l = 0;
        for (File dir : f.listFiles()) {
            if (!dir.isDirectory()) {
                continue;
            }
            String name = dir.getName();
            for (File s : dir.listFiles()) {
                if (!s.isFile()) {
                    continue;
                }
                String fileName = s.getName();
                if (!fileName.endsWith(".mp4") && !fileName.endsWith(".mov")) {
                    continue;
                }
                result.add(new VideoMode(l, name + ":" + s.getName(), s.getAbsolutePath(), false));

            }
        }
        return result;
    }
}
