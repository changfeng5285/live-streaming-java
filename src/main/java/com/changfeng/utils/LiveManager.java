package com.changfeng.utils;

import com.changfeng.mode.VideoMode;
import com.changfeng.service.LiveService;

public class LiveManager {
    private Long id;

    private LibLive instance;

    private final VideoMode videoMode;

    private final LiveService liveService;


    public LiveManager(VideoMode videoMode, LiveService liveService) {
        this.videoMode = videoMode;
        this.liveService = liveService;
        instance = new LibLive();
    }

    public int init(String outPut, String input) {
        id = instance.init(outPut, input);
        return 0;
    }

    public int startPush() {
        if (id == null) {
            return 1;
        }
        new Thread(() -> {
            videoMode.setState(true);
            int i = instance.startPush(id);
            stopPush();
            videoMode.setState(false);
            liveService.resetLiveManager();
        }).start();
        return 0;
    }

    public int stopPush() {
        if (id == null) {
            return 1;
        }
        int i = instance.stopPush(id);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        instance.del(id);
        return i;
    }
}
