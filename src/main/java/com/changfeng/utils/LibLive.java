package com.changfeng.utils;

public class LibLive {

    static {
        System.load("/lib/libLIVE.so");
    }

    //与动态库中的函数相对应
    public native long init(String outPut, String inPut);

    public native int startPush(long ptr);

    public native int stopPush(long ptr);

    public native void del(long ptr);
}
