package com.changfeng.controller;

import com.changfeng.mode.Results;
import com.changfeng.mode.VideoMode;
import com.changfeng.service.LiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping
public class LiveController {

    @Autowired
    private LiveService liveService;


    @PostMapping("/add-remote")
    public Results<String> addOutPut(@PathParam("remoteAddr") String remoteAddr) {
        return liveService.addOutPut(remoteAddr);
    }

    @GetMapping("/get-remote")
    public Results<String> getRemote(){
        return liveService.getRemote();
    }

    @PostMapping("/push")
    public Results<String> push(Integer inPut) {
        System.out.println(inPut);
        return liveService.push(inPut);
    }

    @PostMapping("/stop")
    public Results<String> stop() {
        return liveService.stop();
    }

    @GetMapping("/list")
    public Results<List<VideoMode>> list() {
         return liveService.getVideoList();
//        List<VideoMode> list = new ArrayList<>();
//        for (int i = 0; i < 20; i++) {
//            list.add(new VideoMode("测试名称" + i, "测试地址" + i, false));
//        }
//        return Results.success(list);
    }

//    @GetMapping("/refresh")
//    public Results<List<VideoMode>> refresh() {
//        return liveService.refresh();
//    }
}
