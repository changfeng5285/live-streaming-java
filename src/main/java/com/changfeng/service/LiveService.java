package com.changfeng.service;

import com.changfeng.mode.Results;
import com.changfeng.mode.VideoMode;
import com.changfeng.utils.LiveManager;
import com.changfeng.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LiveService {
    private LiveManager liveManager;
    private String outPut;

    private List<VideoMode> sourceList = Utils.findVideo("/dist");

    public Results<String> push(Integer inPut) {
        if (outPut == null || inPut == null) {
            return Results.error("数据错误");
        }
        if (liveManager != null) {
            return Results.error("有视频正在推流");
        }
        if (sourceList == null || sourceList.size() == 0) {
            return Results.error("未找到视频");
        }
        VideoMode videoMode = null;
        for (VideoMode v : sourceList) {
            if (v.getId() == inPut) {
                videoMode = v;
                break;
            }
        }
        if (videoMode == null) {
            return Results.error("未找到视频");
        }
        liveManager = new LiveManager(videoMode, this);
        liveManager.init(outPut, videoMode.getPath());
        liveManager.startPush();
        return Results.success("推流成功");
    }

    public Results<String> stop() {
        if (liveManager != null) {
            liveManager.stopPush();
        }
        liveManager = null;
        return Results.success("停止成功");
    }

    public Results<String> addOutPut(String remoteAddr) {
        if (remoteAddr == null) {
            return Results.error("地址不能为空");
        }
        if (!remoteAddr.startsWith("rtmp://")) {
            return Results.error("仅支持rtmp协议地址");
        }
        this.outPut = remoteAddr;
        return Results.success("设置成功");
    }

    public void resetLiveManager() {
        this.liveManager = null;
    }

    public Results<List<VideoMode>> getVideoList() {
        if (sourceList == null || sourceList.size() == 0) {
            return Results.error(null);
        }
        return Results.success(sourceList);
    }

    public Results<List<VideoMode>> refresh() {
        sourceList = Utils.findVideo("/dist");
        if (sourceList == null || sourceList.size() == 0) {
            return Results.error(null);
        }
        return Results.success(sourceList);
    }

    public Results<String> getRemote() {
        if (outPut == null) {
            return Results.error();
        }
        return Results.success(outPut);
    }
}
